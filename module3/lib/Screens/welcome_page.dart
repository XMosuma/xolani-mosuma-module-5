import 'package:flutter/material.dart';
import 'package:login_app/Screens/Login.dart';
//import 'package:login_app/Screens/signup.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
      return Scaffold(
      appBar: AppBar(
        title: const Text("Welcome To ShyWay"),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(
            context, MaterialPageRoute(builder: (context) => const LogIn(),)
            ),
        },
        child: const Text("Login"),
        ),
    );
    
  }
}